#!/bin/bash

# Install software
cd
mkdir -p ~/.local/bin

# tmux
sudo yum update && sudo yum install -y tmux

#yq
wget https://github.com/mikefarah/yq/releases/download/v4.45.1/yq_linux_amd64.tar.gz -O -| tar -xz && mv ~/yq_linux_amd64 ~/.local/bin/yq

#jq
wget https://github.com/jqlang/jq/releases/download/jq-1.7.1/jq-linux-amd64 && chmod +x ~/jq-linux64 && mv ~/jq-linux64 ~/.local/bin/jq

#Helm
wget https://get.helm.sh/helm-v3.17.1-linux-amd64.tar.gz -O -|tar -xz && mv ~/linux-amd64/helm ~/.local/bin/helm

#Kustomize
wget https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv5.6.0/kustomize_v5.6.0_linux_amd64.tar.gz -O -| tar -xz && mv kustomize ~/.local/bin/kustomize

# FZF
wget https://github.com/junegunn/fzf/releases/download/v0.56.3/fzf-0.60.0-linux_amd64.tar.gz -O -| tar -xz && mv fzf ~/.local/bin/fzf

#Flux
wget https://github.com/fluxcd/flux2/releases/download/v2.4.0/flux_2.4.0_linux_amd64.tar.gz -O -| tar -xz && mv flux ~/.local/bin/flux

#SOPS
wget https://github.com/getsops/sops/releases/download/v3.9.2/sops-v3.9.4.linux.amd64 -O ~/.local/bin/sops && chmod +x ~/.local/bin/sops

#EKSctl
wget https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_Linux_amd64.tar.gz -O -| tar -xz && mv eksctl ~/.local/bin/eksctl


# Install bash completion
kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl
helm completion bash | sudo tee /etc/bash_completion.d/helm
flux completion bash | sudo tee /etc/bash_completion.d/flux
kustomize completion bash | sudo tee /etc/bash_completion.d/kustomize

# backup .bashrc
cp ~/.bashrc ~/.bashrc.orig

# install dotfiles
#cp ~/.dotfiles/.bashrc ~/
cp ~/.dotfiles/.vimrc ~/
cp ~/.dotfiles/.tmux.conf ~/
